"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _telegraf = _interopRequireDefault(require("telegraf"));

var _summary = require("./summary");

var _DataInfoBot = _interopRequireDefault(require("../models/DataInfoBot"));

var bot = new _telegraf["default"](process.env.BOTTOKEN);
bot["catch"](function (error) {
  console.log('telegraf error', error.response, error.parameters, error.on || error);
});
bot.start(function (ctx) {
  return ctx.reply("Hola ".concat(ctx.from.first_name, ", escribe un pais o escribe la palabra \"global\""));
});
bot.help(function (ctx) {
  ctx.reply('Mandame un sticker');
});
bot.on('sticker', function (ctx) {
  return ctx.reply('👍');
});
bot.hears(['hi', 'hola', 'Hola', 'Hi', 'Ayuda', 'ayuda'], function (ctx) {
  return ctx.replyWithMarkdown("Hola ".concat(ctx.from.first_name, ", ingresa el comando *correpto*, solo dime de que pais quieres saber sobre el *Moustro del Viris*, si no entiendo el pais, escribemelo en *ingles* o escribe global"));
});
bot.on('text', /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(ctx) {
    var data, namePais, doc, total, confirmados, faltan, nametosave, paisFound;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            // const start = new Date()
            ctx.reply('Espera un mommento dejame recordarme...');
            data = {};
            namePais = ctx.message.text.trim().toLowerCase();
            _context.next = 5;
            return _DataInfoBot["default"].findOne({
              idTelegram: ctx.message.from.id
            });

          case 5:
            doc = _context.sent;

            if (doc) {
              _context.next = 10;
              break;
            }

            doc = new _DataInfoBot["default"]({
              idTelegram: ctx.message.from.id,
              nombre: ctx.message.from.first_name + ' ' + ctx.message.from.last_name
            });
            _context.next = 10;
            return doc.save();

          case 10:
            doc.consultas++;
            _context.t0 = namePais;
            _context.next = _context.t0 === 'us' ? 14 : _context.t0 === 'españa' ? 18 : _context.t0 === 'italia' ? 22 : _context.t0 === 'brasil' ? 26 : 30;
            break;

          case 14:
            _context.next = 16;
            return (0, _summary.getStatusCountry)('united-states');

          case 16:
            data = _context.sent;
            return _context.abrupt("break", 34);

          case 18:
            _context.next = 20;
            return (0, _summary.getStatusCountry)('spain');

          case 20:
            data = _context.sent;
            return _context.abrupt("break", 34);

          case 22:
            _context.next = 24;
            return (0, _summary.getStatusCountry)('italy');

          case 24:
            data = _context.sent;
            return _context.abrupt("break", 34);

          case 26:
            _context.next = 28;
            return (0, _summary.getStatusCountry)('brazil');

          case 28:
            data = _context.sent;
            return _context.abrupt("break", 34);

          case 30:
            _context.next = 32;
            return (0, _summary.getStatusCountry)(namePais.replace(' ', '-'));

          case 32:
            data = _context.sent;
            return _context.abrupt("break", 34);

          case 34:
            if (!(data && (data.Country !== undefined || namePais === 'global'))) {
              _context.next = 45;
              break;
            }

            total = data.TotalConfirmed;
            confirmados = data.NewConfirmed;
            faltan = data.TotalConfirmed - data.TotalRecovered - data.TotalDeaths;
            nametosave = data.Country ? data.Country : 'global';
            paisFound = doc.paises.find(function (elem) {
              return elem.nombre === nametosave;
            });

            if (!paisFound) {
              doc.paises = [].concat((0, _toConsumableArray2["default"])(doc.paises), [{
                nombre: nametosave
              }]);
            }

            doc.save();
            return _context.abrupt("return", ctx.replyWithMarkdown("\n    Hoy en *".concat(data.Country ? data.Country : 'el Mundo', "* tenemos\n\n*").concat(Intl.NumberFormat().format(total), "* Total confirmados\n\n*").concat(Intl.NumberFormat().format(confirmados), "* nuevos confirmados\n\n*").concat(Intl.NumberFormat().format(faltan), "* faltan por recuperarse\n    ")));

          case 45:
            doc.save();
            return _context.abrupt("return", ctx.replyWithMarkdown("Perdona, cual pais dices?\nEscribre *Ayuda*"));

          case 47:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}());
var _default = bot;
exports["default"] = _default;
# Bot Covid 19

_Bot que consulta datos del coronavirus por pais_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node JS
File .env que contenga Bot Token
```

_Install_

```
npm install
```

## Ejecutando las pruebas ⚙️

_Ejecutar las pruebas automatizadas para este sistema_

_Dev_
```
npm run dev
```
_Production_
```
npm start
```

## Construido con 🛠️

_Frameworks_

* [Nodejs](https://nodejs.org/es/) - El Framework Back-End

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Kev Pineda** - *Trabajo Inicial y Desarrollo* - [KvP212](https://gitlab.com/KvP212)

También puedes mirar la lista de todos los [contribuyentes](https://gitlab.com/KvP212/bot-telegram-covid-19/-/project_members) quíenes han participado en este proyecto.

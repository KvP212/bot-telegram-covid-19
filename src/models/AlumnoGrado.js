"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var alumnoGradoSchema = new _mongoose.Schema({
  alumnoId: {
    type: String,
    required: true
  },
  gradoId: {
    type: String,
    required: true
  },
  seccion: {
    type: String
  }
});

var _default = (0, _mongoose.model)('AlumnoGrado', alumnoGradoSchema);

exports["default"] = _default;
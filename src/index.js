"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _express = _interopRequireWildcard(require("express"));

var _database = require("./database");

var _morgan = _interopRequireDefault(require("morgan"));

var _cors = _interopRequireDefault(require("cors"));

var _botApp = _interopRequireDefault(require("./bot-app"));

var _dataInfo = _interopRequireDefault(require("./routes/dataInfo.routes"));

function main() {
  return _main.apply(this, arguments);
}

function _main() {
  _main = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
    var app;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _botApp["default"].launch();

            _context.next = 3;
            return (0, _database.tryConnectMongo)();

          case 3:
            app = (0, _express["default"])(); //Settings

            app.set('port', process.env.PORT || 3030); //Middlewares

            app.use((0, _morgan["default"])('dev'));
            app.use((0, _express.json)());
            app.use((0, _cors["default"])()); //Routers

            app.use('/', _dataInfo["default"]); //Starting the server

            app.listen(app.get('port'), function () {
              return console.log("Server port ".concat(app.get('port')));
            });

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _main.apply(this, arguments);
}

main();
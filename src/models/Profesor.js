"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var profesorSchema = new _mongoose.Schema({
  nombre: {
    type: String,
    required: true
  },
  apellidos: {
    type: String
  },
  genero: {
    type: String
  }
});

var _default = (0, _mongoose.model)('Profesor', profesorSchema);

exports["default"] = _default;
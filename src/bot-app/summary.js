"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getStatusCountry = getStatusCountry;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

function getStatusCountry(_x) {
  return _getStatusCountry.apply(this, arguments);
}

function _getStatusCountry() {
  _getStatusCountry = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(name) {
    var dataCountry, dataFetch, data;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            dataCountry = {};
            _context.prev = 1;
            _context.next = 4;
            return (0, _nodeFetch["default"])("https://api.covid19api.com/summary");

          case 4:
            dataFetch = _context.sent;
            _context.next = 7;
            return dataFetch.json();

          case 7:
            data = _context.sent;

            if (name === 'global') {
              dataCountry = data.Global;
            } else {
              dataCountry = data.Countries.find(function (elem) {
                return elem.Slug === name;
              }); // for (let i = 0; i < data.Countries.length; i++) {
              //   if (data.Countries[i].Slug === name) {
              //     dataCountry = data.Countries[i]
              //   }
              // }
            }

            return _context.abrupt("return", dataCountry);

          case 12:
            _context.prev = 12;
            _context.t0 = _context["catch"](1);
            return _context.abrupt("return", dataCountry = null);

          case 15:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 12]]);
  }));
  return _getStatusCountry.apply(this, arguments);
}
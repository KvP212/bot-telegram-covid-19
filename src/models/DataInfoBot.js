"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var paisesSchema = new _mongoose.Schema({
  nombre: {
    type: String
  }
});
var dataInfoSchema = new _mongoose.Schema({
  idTelegram: {
    type: Number,
    required: true
  },
  nombre: {
    type: String
  },
  consultas: {
    type: Number,
    "default": 0
  },
  paises: {
    type: [paisesSchema],
    "default": []
  }
});

var _default = (0, _mongoose.model)('DataInfoBot', dataInfoSchema);

exports["default"] = _default;
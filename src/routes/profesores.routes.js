"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _Profesor = _interopRequireDefault(require("../models/Profesor"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get('/', function _callee(req, res) {
  var alumnosDatas;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(_Profesor["default"].find());

        case 2:
          alumnosDatas = _context.sent;
          res.status(200).json(alumnosDatas);

        case 4:
        case "end":
          return _context.stop();
      }
    }
  });
});
router.post('/create', function _callee2(req, res) {
  var _req$body, nombre, apellidos, genero, newProfesor;

  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _req$body = req.body, nombre = _req$body.nombre, apellidos = _req$body.apellidos, genero = _req$body.genero;
          newProfesor = new _Profesor["default"]({
            nombre: nombre,
            apellidos: apellidos,
            genero: genero
          });
          _context2.next = 4;
          return regeneratorRuntime.awrap(newProfesor.save());

        case 4:
          res.status(200).json({
            status: 'SP',
            description: 'Save Profesor'
          });

        case 5:
        case "end":
          return _context2.stop();
      }
    }
  });
});
router.post('/update', function _callee3(req, res) {
  var _req$body2, id, nombre, apellidos, genero;

  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _req$body2 = req.body, id = _req$body2.id, nombre = _req$body2.nombre, apellidos = _req$body2.apellidos, genero = _req$body2.genero;
          _context3.next = 3;
          return regeneratorRuntime.awrap(_Profesor["default"].findByIdAndUpdate({
            _id: id
          }, {
            nombre: nombre,
            apellidos: apellidos,
            genero: genero
          }, function (err, resDoc) {
            if (err) console.log('Error:', err);
          }));

        case 3:
          res.status(200).json({
            status: 'UP',
            description: 'Update Profesor'
          });

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  });
});
router.post('/delete', function _callee4(req, res) {
  var id;
  return regeneratorRuntime.async(function _callee4$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          id = req.body.id;
          _context4.next = 3;
          return regeneratorRuntime.awrap(_Profesor["default"].findByIdAndDelete({
            _id: id
          }));

        case 3:
          res.status(200).json({
            status: 'DP',
            description: 'Delete Profesor'
          });

        case 4:
        case "end":
          return _context4.stop();
      }
    }
  });
});
var _default = router;
exports["default"] = _default;
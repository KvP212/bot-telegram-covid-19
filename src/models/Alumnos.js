"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var alumnosSchema = new _mongoose.Schema({
  nombre: {
    type: String,
    required: true
  },
  apellidos: {
    type: String
  },
  genero: {
    type: String
  },
  fechaNacimiento: {
    type: Date
  }
});

var _default = (0, _mongoose.model)('Alumnos', alumnosSchema);

exports["default"] = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var gradoSchema = new _mongoose.Schema({
  nombre: {
    type: String,
    required: true
  },
  profesorId: {
    type: String,
    required: true
  }
});

var _default = (0, _mongoose.model)('Grados', gradoSchema);

exports["default"] = _default;
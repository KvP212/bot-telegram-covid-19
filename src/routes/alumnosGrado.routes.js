"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _AlumnoGrado = _interopRequireDefault(require("../models/AlumnoGrado"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get('/', function _callee(req, res) {
  var alumnosGradossDatas;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(_AlumnoGrado["default"].find());

        case 2:
          alumnosGradossDatas = _context.sent;
          res.status(200).json(alumnosGradossDatas);

        case 4:
        case "end":
          return _context.stop();
      }
    }
  });
});
router.post('/create', function _callee2(req, res) {
  var _req$body, alumnoId, gradoId, seccion, newAlumnoGrado;

  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _req$body = req.body, alumnoId = _req$body.alumnoId, gradoId = _req$body.gradoId, seccion = _req$body.seccion;
          newAlumnoGrado = new _AlumnoGrado["default"]({
            alumnoId: alumnoId,
            gradoId: gradoId,
            seccion: seccion
          });
          _context2.next = 4;
          return regeneratorRuntime.awrap(newAlumnoGrado.save());

        case 4:
          res.status(200).json({
            status: 'SAG',
            description: 'Save AlumnoGrado'
          });

        case 5:
        case "end":
          return _context2.stop();
      }
    }
  });
});
router.post('/update', function _callee3(req, res) {
  var _req$body2, id, alumnoId, gradoId, seccion;

  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _req$body2 = req.body, id = _req$body2.id, alumnoId = _req$body2.alumnoId, gradoId = _req$body2.gradoId, seccion = _req$body2.seccion;
          _context3.next = 3;
          return regeneratorRuntime.awrap(_AlumnoGrado["default"].findByIdAndUpdate({
            _id: id
          }, {
            alumnoId: alumnoId,
            gradoId: gradoId,
            seccion: seccion
          }, function (err, resDoc) {
            if (err) console.log('Error:', err);
          }));

        case 3:
          res.status(200).json({
            status: 'UAG',
            description: 'Update AlumnoGrado'
          });

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  });
});
router.post('/delete', function _callee4(req, res) {
  var id;
  return regeneratorRuntime.async(function _callee4$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          id = req.body.id;
          _context4.next = 3;
          return regeneratorRuntime.awrap(_AlumnoGrado["default"].findByIdAndDelete({
            _id: id
          }));

        case 3:
          res.status(200).json({
            status: 'DAG',
            description: 'Delete AlumnoGrado'
          });

        case 4:
        case "end":
          return _context4.stop();
      }
    }
  });
});
var _default = router;
exports["default"] = _default;